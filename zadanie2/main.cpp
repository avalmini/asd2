#include <iostream>
#include <fstream>

using namespace std;

void load(char** &tab,int &n,int &m){
    char tmp;
    ifstream wejscie;
    ofstream wyjscie;
    wejscie.open("duzy1.txt");
    wyjscie.open("out.txt");
    wejscie>>m>>n;
    tab = new char*[n];
    for(int i = 0; i < n; i++){
        tab[i] = new char[m];
    }

    for(int i=0;i<n;i++){
        for(int j=0;j<m;j++){
            tmp=wejscie.get();
            if(tmp!='\n'){
                tab[i][j]=tmp;
            }
            else{
                tab[i][j]=wejscie.get();
            }
        }
    }

}

int river(char** tab,char** tmp,int n,int m,int i,int j,int &op){
    int wynik=0,wynikmax=0;
    tmp[i][j] = '0';
        if(tmp[i-1][j]=='u'){
                wynik=river(tab,tmp,n,m,i-1,j,op);
                if(wynik>wynikmax)wynikmax=wynik;
        }
        if(tmp[i+1][j]=='u'){
                wynik=river(tab,tmp,n,m,i+1,j,op);
                if(wynik>wynikmax)wynikmax=wynik;
        }
        if(tmp[i][j-1]=='u'){
                wynik=river(tab,tmp,n,m,i,j-1,op);
                if(wynik>wynikmax)wynikmax=wynik;
        }
        if(tmp[i][j+1]=='u'){
                wynik=river(tab,tmp,n,m,i,j+1,op);
                if(wynik>wynikmax)wynikmax=wynik;
        }
    op++;
    return wynikmax+1;

}

int island(char** tab,char** tmp,int n,int m,int i,int j,int &border,int &op){
    int wynik=0;
    tmp[i][j] = '0';
    for(int a=i-1;a<=i+1;a++){
        for(int b=j-1;b<=j+1;b++){
            if(tmp[a][b]=='b'){
                border=1;
            }
            if(tmp[a][b]=='x'){
                wynik+=island(tab,tmp,n,m,a,b,border,op);
            }
        }
    }
    if(border==1)return 0;
    op++;
    return wynik+1;
}

void alg(char** tab,int n,int m,int &op){
    ofstream wyjscie;
    wyjscie.open("out.txt");

    int wynik=0,wynikmaxriver=0,wynikmaxisland=0,border=0;
    char** tmp=new char*[n+2];        //alokowanie tablicy pomocniczej
    for(int i = 0; i < n+2; i++){
        tmp[i] = new char[m+2];
    }


    tmp[0][0]='b';
    tmp[n+1][0]='b';
    tmp[n+1][m+1]='b';
    tmp[0][m+1]='b';

    for(int i=1;i<n+1;i++){
        for(int j=1;j<m+1;j++){
            tmp[i][j]=tab[i-1][j-1];
            tmp[0][j]='b';
            tmp[i][0]='b';
            tmp[n+1][j]='b';
            tmp[i][m+1]='b';

        }
    }

    for(int i=1;i<(n+1)/2+1;i++){
        for(int j=1;j<m+1;j++){
            if(tmp[i][j]=='x'){
                    wynik = island(tab,tmp,n,m,i,j,border,op);
                    border=0;
                    if(wynik>wynikmaxisland)wynikmaxisland=wynik;
            }
        }
    }
    cout<<wynikmaxisland<<endl;
    wyjscie<<wynikmaxisland<<" ";



    for(int i=1;i<n+1;i++){
        for(int j=1;j<m+1;j++){
            if(tmp[i][j]=='u')
                if(tmp[i-1][j]=='o' ||
                   tmp[i+1][j]=='o' ||
                   tmp[i][j-1]=='o' ||
                   tmp[i][j+1]=='o'){
                    wynik = river(tab,tmp,n,m,i,j,op);
                    if(wynik>wynikmaxriver)wynikmaxriver=wynik;
                   }
        }
    }
    cout<<wynikmaxriver<<endl;
    wyjscie<<wynikmaxriver;
}
int main()
{
    int n,m,op=0;
    char** tab;
    load(tab,n,m);
    alg(tab,n,m,op);
    cout<<"licznik operacji: "<<op;
}
